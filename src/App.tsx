import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import Menu from "./Menu";
import CreateRoute from "./pages/CreateRoute";
import Home from "./pages/Home";
import Routes from "./pages/Routes";
import SignIn from "./pages/SignIn";
import RouteDetail from "./pages/RouteDetail";
import Users from "./pages/Users";
import { ProvideAuth } from "./useAuth";

//@ts-ignore
//eslint-disable-next-line import/no-webpack-loader-syntax
import mapboxgl from "!mapbox-gl";
import SignUp from "./pages/SignUp";
import VerifyEmail from "./pages/VerifyEmail";
import EmailVerificationRequest from "./components/EmailVerificationRequest.component";
import PasswordForgot from "./pages/PasswordForgot";
import PasswordReset from "./pages/PasswordReset";
mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_TOKEN!;

function App() {

  return (
    <div className="App">
      <ProvideAuth>
        <Router>
          <Menu></Menu>
          <div className="p-4">
            <EmailVerificationRequest></EmailVerificationRequest>
            <Switch>
              <Route path="/users">
                <Users />
              </Route>
              <Route path="/routes">
                <Routes />
              </Route>
              <Route path="/route/:id" component={RouteDetail} />
              <Route path="/create-route">
                <CreateRoute />
              </Route>
              <Route path="/sign-in">
                <SignIn />
              </Route>
              <Route path="/password-forgot">
                <PasswordForgot />
              </Route>
              <Route path="/sign-up">
                <SignUp />
              </Route>
              <Route path="/verify-email/:identity/:today/:token" component={VerifyEmail} />
              <Route path="/password-reset/:identity/:today/:token" component={PasswordReset} />
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </div>
        </Router>
      </ProvideAuth>
    </div>
  );
}

export default App;
