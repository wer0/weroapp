import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { useAuth } from "./useAuth";

function Menu() {
  const auth = useAuth();

  // useEffect(() => {
  //   auth.retrieveUser();
  // }, [])

  return (
    <div className="flexbox">
      <nav className="bg-blue-500 shadow-lg p-2 pl-3 select-none text-white flex items-center">
        <ul className="inline-block">
          <li className="font-semibold inline-block mr-3 hover:opacity-80 transition-all">
            <Link to="/">Wero</Link>
          </li>
          <li className="inline-block mr-2 hover:opacity-80 transition-all">
            <Link to="/routes">Routes</Link>
          </li>
          <li className="inline-block mr-2 hover:opacity-80 transition-all">
            <Link to="/users">Users</Link>
          </li>
          {!auth.user ? (
            <React.Fragment>
              <li className="inline-block mr-2 hover:opacity-80 transition-all">
                <Link to="/sign-in">Sign In</Link>
              </li>
              <li className="inline-block mr-2 hover:opacity-80 transition-all">
                <Link to="/sign-up">Sign Up</Link>
              </li>
            </React.Fragment>
          ) : (
            <Fragment />
          )}
        </ul>
        {auth.user && (
          <div className="ml-auto">
            <span className="text-sm italic mr-2">Signed in as</span>
            <Link className="underline" to={""}>
              {auth.user.username}
            </Link>
            <button
              className="ml-2 cursor-pointer bg-blue-700 hover:bg-blue-600 rounded p-2 py-1"
              onClick={() => auth.signOut()}
            >
              Sign out
            </button>
          </div>
        )}
      </nav>
    </div>
  );
}

export default Menu;
