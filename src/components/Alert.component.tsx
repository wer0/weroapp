import { ReactChild } from "react";

export function Alert ({title, children}: { title: string, children: ReactChild }) {
    return (
        <div className="p-4 bg-red-100 text-red-900 rounded mb-4">
            <b>{title}</b>
            <p>{children}</p>
        </div>
    )
}