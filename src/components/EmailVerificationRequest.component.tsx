import React, { useState } from "react";
import { useAuth } from "../useAuth";
import { Alert } from "./Alert.component";
import { resendVerificationEmail } from '../weroServices'; 

export default function EmailVerificationRequest () {

    const auth = useAuth();
    const [sent, setSent] = useState(false);

    const request = async () => {
        await resendVerificationEmail();
        setSent(true);
    }

    if (auth.user && !auth.user.emailVerified) {
        return (
            <Alert title="Email not verified">
                <React.Fragment>
                    Your email address has not been verified. Check your inbox and spam folder for our email. We can also send you a new email.
                    <br></br>
                    { !sent ? (
                        <button onClick={() => request()} className="bg-red-500 text-white py-1 px-2 rounded mt-3">Re-send verification email</button>
                    ) : (
                        <span>Email sent!</span>
                    ) }
                </React.Fragment>
            </Alert>
        )
    } else {
        return (<React.Fragment/>)
    }
        
    
}