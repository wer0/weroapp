import { Link } from "react-router-dom";

export default function RouteComponent({
  routeId,
  routeName,
  routeDescription,
}: {
  routeId: string;
  routeName: string;
  routeDescription: string;
}) {
  return (
    <Link
      to={`route/${routeId}`}
      className="block w-full p-6 mx-auto rounded-xl shadow-md bg-gray-100 mb-6 select-none cursor-pointer hover:bg-gray-200"
    >
      <h1 className="text-xl font-medium text-black">{routeName}</h1>
      <p className="text-gray-500">{routeDescription}</p>
    </Link>
  );
}
