//@ts-ignore
//eslint-disable-next-line import/no-webpack-loader-syntax
import mapboxgl, { LngLat, Map } from "!mapbox-gl";
import { useEffect, useRef, useState } from "react";
import "mapbox-gl/dist/mapbox-gl.css";

export default function SimpleMap(props: {
  geoJSON: string;
  distance: number;
}) {
  const mapContainer = useRef<any>(null);
  const map = useRef<Map>();
  const [mapStyle, setMapStyle] = useState("outdoors");
  const [fullscreen, setFullscreen] = useState(false);
  const geoJSON = useRef<any>(undefined);

  useEffect(() => {
    map.current?.resize()
  }, [fullscreen])

  useEffect(() => {
    if (map.current) return; // initialize map only once
    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/outdoors-v11",
      center: [0, 0],
      zoom: 1,
    });
  });

  useEffect(() => {
    if (props.geoJSON) geoJSON.current = JSON.parse(props.geoJSON);
    if (geoJSON.current) {
      const coordinatesSum =
        geoJSON.current.features[0].geometry.coordinates.reduce(
          (a: any, b: any) => [a[0] + b[0], a[1] + b[1]],
          [0, 0]
        );
      const lngAverage =
        coordinatesSum[0] /
        geoJSON.current.features[0].geometry.coordinates.length;
      const latAverage =
        coordinatesSum[1] /
        geoJSON.current.features[0].geometry.coordinates.length;
      map.current?.setCenter(new LngLat(lngAverage, latAverage));
      map.current?.setZoom(props.distance > 100 * 1000 ? 8 : 10);
      if (map.current?.isStyleLoaded() === true) {
        console.log("style is supposed to be loaded");
        addGeoJSON();
      } else {
        console.log("subscribing to data event for a really ready map");
        map.current?.once("data", () => {
          console.log("fired!");
          addGeoJSON();
        });
      }
    }
  }, [props]);

  const addGeoJSON = () => {
    if (!map.current?.getSource("route"))
      map.current?.addSource("route", {
        type: "geojson",
        data: geoJSON.current,
      });

    if (!map.current?.getLayer("route"))
      map.current?.addLayer({
        id: "route",
        type: "line",
        source: "route",
        layout: {
          "line-join": "round",
          "line-cap": "round",
        },
        paint: {
          "line-color": "#0a0fc9",
          "line-width": 5,
        },
      });
  };

  const setStyle = (satellite: boolean) => {
    setMapStyle(satellite ? "satellite" : "outdoors");
    map.current?.setStyle(
      satellite
        ? "mapbox://styles/mapbox/satellite-streets-v11"
        : "mapbox://styles/mapbox/outdoors-v11"
    );
    map.current?.once("style.load", () => addGeoJSON());
  };

  return (
    // <div className="h-96 w-full">
    <div className={!fullscreen ? 'h-96 w-full' : 'h-full w-full fixed top-0 left-0'}>
        <div className="absolute z-50 m-2 p-1 bg-white bg-opacity-40 shadow rounded">
          {mapStyle === "outdoors" ? (
            <button
              className="bg-blue-500 text-white p-2 pl-3 pr-3 inline-flex items-center rounded"
              onClick={() => setStyle(true)}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-4 w-4"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z"
                  clipRule="evenodd"
                />
              </svg>
            </button>
          ) : (
            <button
              className="bg-blue-500 text-white p-2 pl-3 pr-3 inline-flex items-center rounded"
              onClick={() => setStyle(false)}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-4 w-4"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"
                />
              </svg>
            </button>
          )}
          {fullscreen ? (
            <button
              className="bg-blue-500 text-white p-2 pl-3 pr-3 inline-flex items-center rounded ml-1"
              onClick={() => setFullscreen(false)}
            >
              <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                <path stroke-linecap="round" stroke-linejoin="round" d="M4 8V4m0 0h4M4 4l5 5m11-1V4m0 0h-4m4 0l-5 5M4 16v4m0 0h4m-4 0l5-5m11 5l-5-5m5 5v-4m0 4h-4" />
              </svg>
            </button>
          ) : (
            <button
              className="bg-blue-500 text-white p-2 pl-3 pr-3 inline-flex items-center rounded ml-1"
              onClick={() => setFullscreen(true)}
            >
              <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                <path stroke-linecap="round" stroke-linejoin="round" d="M4 8V4m0 0h4M4 4l5 5m11-1V4m0 0h-4m4 0l-5 5M4 16v4m0 0h4m-4 0l5-5m11 5l-5-5m5 5v-4m0 4h-4" />
              </svg>
            </button>
          )}
        </div>
      <div ref={mapContainer} className={'map-container h-full w-full relative'}>
      </div>
    </div>
  );
}
