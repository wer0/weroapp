import IUser from "./IUser";

export default interface IRoute {
  id: number;
  name: string;
  description: string;
  gpx?: string;
  geoJSON?: string;
  user?: IUser;
  distance?: number;
  elevationGain?: number;
  rating?:number;
}
