export default interface IUser {
  username: string;
  email: string;
  id: number;
  emailVerified: boolean;
}
