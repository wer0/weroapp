import { FormEvent, useState } from "react";
import { useHistory } from "react-router-dom";
import { createRoute, uploadGpxToRoute } from "../weroServices";

export default function CreateRoute() {
  const history = useHistory();

  const [name, setName] = useState<string>("");
  const [description, setDescription] = useState<string>("");
  const [gpxFile, setGpxFile] = useState<File>();

  const submit = async (event: FormEvent) => {
    event.preventDefault();
    const newRoute = await createRoute(name, description, 1);
    if (gpxFile) await uploadGpxToRoute(gpxFile, newRoute.id);
    history.push(`/route/${newRoute.id}`);
  };

  return (
    <div>
      <header className="flex mb-5">
        <h2 className="text-2xl">Create Route</h2>
      </header>
      <hr />
      <form onSubmit={submit}>
        <div className="mb-4 mt-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="name"
          >
            Route name
          </label>
          <input
            required
            value={name}
            onChange={(event) => setName(event.target.value)}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="name"
            type="text"
            placeholder="Route name"
          />
        </div>
        <div className="mb-4 mt-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="description"
          >
            Route description
          </label>
          <textarea
            required
            value={description}
            onChange={(event) => setDescription(event.target.value)}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="description"
            placeholder="Route Description"
          />
        </div>
        <div className="mb-4 mt-4">
          <label
            htmlFor="formFile"
            className="block text-gray-700 text-sm font-bold mb-2"
          >
            GPX File
          </label>
          <input
            required
            onChange={(event) => setGpxFile(event.target.files![0])}
            className="form-control block w-full px-2 py-1.5 text-base font-normal text-gray-700 shadow appearance-none rounded transition ease-in-out m-0 focus:outline-none focus:shadow-outline border-solid border-2"
            type="file"
            id="formFile"
          />
        </div>
        <button
          type="submit"
          className="ml-auto bg-green-500 text-white p-2 pl-3 pr-3 inline-flex items-center rounded mt-4"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="mr-2 h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={4}
              d="M12 4v16m8-8H4"
            />
          </svg>
          <span>Create</span>
        </button>
      </form>
    </div>
  );
}
