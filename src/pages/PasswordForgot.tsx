import { FormEvent, useState } from "react";
import { passwordForgot } from "../weroServices";

export default function PasswordForgot() {
  const [email, setEmail] = useState<string>("");
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);

  const submit = async (event: FormEvent) => {
    event.preventDefault();
    try {
        await passwordForgot(email);
        setSuccess(true);
        setError(false);
    } catch (err) {
        setError(true);
    }
  };

  return (
    <div className="bg-white rounded m-10 shadow-lg p-10">
      <h2 className="text-2xl mb-6">Forgot your password?</h2>
      <form onSubmit={submit} className="mb-6">
        <input
          placeholder="Email"
          className="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker mb-4"
          type="text"
          value={email}
          onChange={(event) => setEmail(event.target.value)}
        ></input>
        <button
          className="p-4 py-3 text-white bg-blue-500 rounded shadow"
          type="submit"
        >
          Send an email
        </button>
      </form>
      { error && (
          <div>Error while trying to send a password reset email.</div>
      ) }
      { success && (
          <div>We sent you an email to reset your password. Check your inbox!</div>
      ) }
    </div>
  );
}
