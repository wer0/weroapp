import React, { FormEvent, useState } from "react";
import { Link, RouteComponentProps } from "react-router-dom";
import { passwordReset } from "../weroServices";

export default function PasswordReset(
  props: RouteComponentProps<{ identity: string; today: string; token: string }>
) {
  const [password, setPassword] = useState("");
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);

  const submit = (event: FormEvent) => {
    event.preventDefault();
    const { identity, today, token } = props.match.params;
    passwordReset(identity, today, token, password)
      .then(() => {
        setSuccess(true);
        setError(false);
      })
      .catch(() => {
        setSuccess(false);
        setError(true);
      });
  };

  return (
    <div className="rounded shadow-lg m-10 p-10">
      <h2 className="text-2xl mb-6">Forgot your password?</h2>
      <form onSubmit={submit} className="mb-6">
        <input
          placeholder="New password"
          className="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker mb-4"
          type="text"
          value={password}
          onChange={(event) => setPassword(event.target.value)}
        ></input>
        <button
          className="p-4 py-3 text-white bg-blue-500 rounded shadow"
          type="submit"
        >
          Reset password
        </button>
      </form>
      {success && !error && (
        <React.Fragment>
          Password reset!
          <br />
          <Link to="/sign-in" className="text-blue-500 underline">
            Sign In
          </Link>
        </React.Fragment>
      )}
      {error && (
        <React.Fragment>Error! Could not reset password.</React.Fragment>
      )}
    </div>
  );
}
