import { useEffect, useState } from "react";
import { RouteComponentProps } from "react-router-dom";
import Rate from "../components/Rate.component";
import SimpleMap from "../components/SimpleMap.component";
import { getRoute, rateRoute } from "../weroServices";

import IRoute from "./../interfaces/IRoute";

export default function RouteDetail(
  props: RouteComponentProps<{ id: string }>
) {
  const [route, setRoute] = useState<IRoute>();

  useEffect(() => {
    getRoute(Number(props.match.params.id)).then((route: IRoute) => {
      setRoute(route);
    });
  }, [props]);

  const handleRating = (rating: number) => {
    rateRoute(Number(props.match.params.id), rating).then(() => {
      getRoute(Number(props.match.params.id)).then((route: IRoute) => {
        setRoute(route);
      });
    })
  }

  return (
    <div>
      <div className="bg-white shadow-lg rounded overflow-hidden">
        <SimpleMap
          geoJSON={route?.geoJSON!}
          distance={route?.distance!}
        ></SimpleMap>
        <div className="flex">
          <div>
            <h1 className="text-xl font-medium p-3 pb-0">{route?.name}</h1>
            <p className="text-gray-500 p-3 pt-0 pb-0">{route?.description}</p>
          </div>
          <div className="ml-auto p-3">
            <i className="text-gray-500 text-sm mr-2">Created by</i>
            {route?.user?.username}
          </div>
        </div>
        <p className="p-3 text-gray-700 flex items-baseline flex-wrap">
          <span className="text-lg font-medium mr-1">
            {Math.round(route?.distance! / 1000)}{" "}
            <span className="font-normal">km</span>
          </span>{" "}
          <i className="text-gray-400 mr-4">Distance</i>
          <span className="text-lg font-medium mr-1">
            {route?.elevationGain!} <span className="font-normal">m </span>
          </span>{" "}
          <i className="text-gray-400 mr-2">Elevation gain</i>
          <div className="ml-0 sm:ml-auto">
            <Rate onRate={handleRating} currentRate={route?.rating}></Rate>
          </div>
        </p>
      </div>
    </div>
  );
}
