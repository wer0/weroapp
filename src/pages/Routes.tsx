import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import RouteComponent from "../components/Route.component";
import IRoute from "../interfaces/IRoute";
import { getRoutes } from "../weroServices";

export default function Routes() {
  const [routes, setRoutes] = useState<IRoute[]>([]);

  useEffect(() => {
    getRoutes().then((data) => setRoutes(data));
  }, []);

  return (
    <div>
      <header className="flex mb-5">
        <h2 className="text-2xl">Routes</h2>
        <Link
          to="/create-route"
          className="ml-auto bg-green-500 text-white p-2 pl-3 pr-3 inline-flex items-center rounded"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="mr-2 h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={4}
              d="M12 4v16m8-8H4"
            />
          </svg>
          <span>New Route</span>
        </Link>
      </header>
      <hr className="mb-5" />
      {routes.map((route) => (
        <RouteComponent
          routeId={String(route.id)}
          routeName={route.name}
          routeDescription={route.description}
          key={route.id}
        ></RouteComponent>
      ))}
    </div>
  );
}
