import { FormEvent, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { useAuth } from "../useAuth";

export default function SignIn() {
  const auth = useAuth();
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const history = useHistory();

  const submit = async (event: FormEvent) => {
    event.preventDefault();
    try {
      await auth.signIn(username, password);
      history.push("/routes");
    } catch (err) {
      console.log(err)
    }
  };

  return (
    <div className="bg-white rounded m-10 shadow-lg p-10">
      <h2 className="text-2xl mb-6">Sign in</h2>
      <form onSubmit={submit} className="mb-6">
        <input
          placeholder="Username"
          className="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker mb-4"
          type="text"
          value={username}
          onChange={(event) => setUsername(event.target.value)}
        ></input>
        <input
          placeholder="Password"
          className="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker mb-4"
          type="password"
          value={password}
          onChange={(event) => setPassword(event.target.value)}
        ></input>
        <button
          className="p-4 py-3 text-white bg-blue-500 rounded shadow"
          type="submit"
        >
          Sign in
        </button>
      </form>
      { auth.authError && (
        <p>We couldn't log you in, check your username and/or password.</p>
      ) }
      <Link to="/password-forgot" className="text-blue-500 underline">Forgot your password?</Link>
    </div>
  );
}
