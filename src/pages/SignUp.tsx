import { FormEvent, useState } from "react";
import { signUp } from "../weroServices";

export default function SignUp() {
  const [username, setUsername] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);

  const submit = async (event: FormEvent) => {
    event.preventDefault();
    try { 
      setLoading(true)
      await signUp(email, username, password);
      setLoading(false)
      setSuccess(true)
    } catch (err) {
      setError(true)
      setLoading(false)
      setTimeout(() => { setError(false) }, 3000)
    }
  };

  return (
    <div className="bg-white rounded m-10 shadow-lg p-10">
      <h2 className="text-2xl mb-6">Sign up</h2>
      <form className="mb-6" onSubmit={submit}>
        <input
          placeholder="Username"
          className="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker mb-4"
          type="text"
          value={username}
          onChange={(event) => setUsername(event.target.value)}
        ></input>
        <input
          placeholder="Email"
          className="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker mb-4"
          type="text"
          value={email}
          onChange={(event) => setEmail(event.target.value)}
        ></input>
        <input
          placeholder="Password"
          className="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker mb-4"
          type="password"
          value={password}
          onChange={(event) => setPassword(event.target.value)}
        ></input>
        <button
          className="p-4 py-3 text-white bg-blue-500 rounded shadow"
          type="submit"
        >
          Sign up
        </button>
      </form>
      {loading && (
        <p>Loading...</p>
      )}
      {error && (
        <p>Error while creating account, your email and username might already be taken.</p>
      )}
      {success && (
        <p>Account created successfuly! Take a look to your inbox, we sent you an email for verification.</p>
      )}
    </div>
  );
}
