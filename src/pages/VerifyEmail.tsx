import React, { useEffect, useState } from "react"
import { Link, RouteComponentProps } from "react-router-dom"
import { verifyEmail } from "../weroServices";

export default function VerifyEmail (props: RouteComponentProps<{ identity: string, today: string, token: string }>) {

    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);

    useEffect(() => {
        const { identity, today, token } = props.match.params;
        verifyEmail(identity, today, token).then(() => {
            setLoading(false);
        }).catch(() => {
            setError(true);
        });
    }, [props])

    return (
        <div className="rounded shadow-lg p-2">
            { loading && !error && (
                <React.Fragment>Loading...</React.Fragment>
            ) }
            { !loading && !error && (
                <React.Fragment>
                    Email verified!
                    <br/>
                    <Link to="/sign-in" className="text-blue-500 underline">Sign In</Link>
                </React.Fragment>
            ) }
            { error && (
                <React.Fragment>Error! Could not verify email.</React.Fragment>
            ) }
        </div>
    )
}