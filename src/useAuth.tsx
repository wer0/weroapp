import React, {
  useState,
  useContext,
  createContext,
  useEffect
} from "react";
import * as weroServices from "./weroServices";

type authContextType = {
  user: { username: string, emailVerified: boolean } | null;
  authError: Error | null;
  signIn: (username: string, password: string) => Promise<void>;
  signOut: () => void;
};

const authContextDefaultValues: authContextType = {
  user: null,
  authError: null,
  signIn: async () => { console.log('initial signi') },
  signOut: () => {}
};

const authContext = createContext(authContextDefaultValues);

export function ProvideAuth({ children }: { children: any }) {
  const auth = useProvideAuth();
  return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

export const useAuth = () => {
  return useContext(authContext);
};

function useProvideAuth() {
  const [user, setUser] = useState<{ username: string, emailVerified: boolean } | null>(null);
  const [authError, setAuthError] = useState<Error | null>(null);

  useEffect(() => {
    weroServices.retrieveUserFromCookie().then((user) => {
      if (user) {
        setUser(user);
      }
    });
  }, []);

  const signIn = async (username: string, password: string) => {
    try {
      const user = await weroServices.signIn(username, password);
      setUser({ username: user.username, emailVerified: user.emailVerified });
      setAuthError(null);      
    } catch (error: any) {
      signOut();
      setAuthError(error);
      throw new Error('Could not login')
    }
  };

  const signOut = () => {
    document.cookie = "payload=; Max-Age=-99999999;";
    setUser(null);
  };

  return { user, authError, signIn, signOut };
}