import axios from "axios";
import IRoute from "./interfaces/IRoute";
import IUser from "./interfaces/IUser";

const BASE_URL = process.env.REACT_APP_SERVICES_BASE_URL;

type JWTPayload = {
  sub: number;
  iat: number;
  exp: number;
};

export async function signIn(
  username: string,
  password: string
): Promise<{ token: string; expires: string, username: string, emailVerified: boolean }> {
  const {
    data: { token, expires, emailVerified },
  } = await axios.post(
    `${BASE_URL}/login`,
    { username, password },
    { withCredentials: true }
  );
  return { token, expires, username, emailVerified };
}

export async function signUp(
  email: string,
  username: string,
  password: string
): Promise<{ username: string; id: number; token: string }> {
  const {
    data: { id, token },
  } = await axios.post(
    `${BASE_URL}/user`,
    { email, username, password }
  );
  return { username, id, token };
}

export async function passwordForgot(
  email: string
): Promise<void> {
  await axios.post(
    `${BASE_URL}/password-forgot`,
    { email }
  );
  return;
}

export async function passwordReset(
  identity: string,
  today: string,
  token: string,
  password: string
): Promise<void> {
  await axios.post(
    `${BASE_URL}/password-reset/${identity}/${today}/${token}`, {
      password
    }
  );
}

export async function verifyEmail(
  identity: string,
  today: string,
  token: string
): Promise<void> {
  await axios.get(
    `${BASE_URL}/verify-email/${identity}/${today}/${token}`
  );
}

export async function resendVerificationEmail(): Promise<void> {
  await axios.get(
    `${BASE_URL}/resend-verification-email`, { withCredentials: true }
  );
}

export async function retrieveUserFromCookie(): Promise<IUser | void> {
  const cookie = getCookie("payload");
  if (!cookie) return;
  
  try {
    const payload: JWTPayload = JSON.parse(atob(cookie.split(".")[1]));
    const user = await getUser(payload.sub);
    return user
  } catch (err) {
    return;
  }
}

export async function getUser(id: number): Promise<IUser> {
  const { data } = await axios.get(`${BASE_URL}/user/${id}`, {
    withCredentials: true,
  });
  return data;
}

export async function getRoutes(): Promise<IRoute[]> {
  const { data } = await axios.get(`${BASE_URL}/route`, {
    withCredentials: true,
  });
  return data;
}

export async function getRoute(id: number): Promise<IRoute> {
  const { data } = await axios.get(`${BASE_URL}/route/${id}`, {
    withCredentials: true,
  });
  return data;
}

export async function rateRoute(id: number, rating: number): Promise<IRoute> {
  const { data } = await axios.post(`${BASE_URL}/route/${id}/rating`, {
    rating
  }, { withCredentials: true });
  return data;
}

export async function createRoute(
  name: string,
  description: string,
  userId: number
): Promise<IRoute> {
  const { data } = await axios.post(
    `${BASE_URL}/route`,
    {
      name,
      description,
      userId,
    },
    { withCredentials: true }
  );
  return data;
}

export async function uploadGpxToRoute(
  file: File,
  routeId: number
): Promise<void> {
  const formData = new FormData();
  formData.append("file", file);
  const { data } = await axios.post(
    `${BASE_URL}/route/${routeId}/gpx-upload`,
    formData,
    { withCredentials: true }
  );
  return data;
}

function getCookie(name: string): string | void {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop()!.split(";").shift();
}